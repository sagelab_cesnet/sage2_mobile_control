# SAGE2 mobile control #

## Description ##

A SAGE2 server addon to provide a simple way for the user to access and manipulate SAGE2 applications running from mobile devices. Touch control and gesture support is integrated using Hammer.js library.

## Features ##

* simultaneously view and control running SAGE2 applications
* replicating SAGE2 pointer events
* pinch to zoom and pan gestures
* press, release and double tap touch events

## Installation ##

* copy mobile.html to ./public/ directory
* copy SAGE2_Mobile.js to ./public/src/ directory

## Use ##

* on mobile web browser open: server:port/mobile.html (e.g., https://sage2.cesnet.cz:10443/mobile.html)
* toggle Fullscreen mode

## Upcoming features ##

* SAGE2 widget support
* iOS device support

## Contact ##

Jiri Kubista, CESNET, Jiri.Kubista@cesnet.cz

![mobile_sage2.png](https://bitbucket.org/repo/paRG96/images/218675399-mobile_sage2.png)