// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014-15

"use strict";

/**
 * SAGE2 Mobile controller
 *
 * @module client
 * @submodule SAGE2_Mobile.js
 * @class SAGE2_Mobile
 */

window.URL = (window.URL || window.webkitURL || window.msURL || window.oURL);

var clientID;
var wsio;

var isMaster;
var hostAlias = {};

var itemCount = 0;
var controlItems   = {};
var controlObjects = {};
var lockedControlElements = {};
var widgetConnectorRequestList = {};

var applications = {};
var dependencies = {};
var dataSharingPortals = {};

// UI object to build the element on the wall
var ui;
var uiTimer = null;
var uiTimerDelay;

// keep ratio of applications same as on other displays (true) or use full mobile device resolution
// not used,  will be replaced by: (keep full resolution (and scale) for complete synchronization or use mobile resolution for better performance)
// var keepRatio = false;

var pointerPos = {};
pointerPos.top = 0;
pointerPos.left = 0;

var pointerSpeed = 15;

var hammertime;
var mc;

var buttonHeight = 30;

var pressed = false;

var scale;
var lastMove;

// Explicitely close web socket when web browser is closed
window.onbeforeunload = function() {
  onExit();
  if (wsio !== undefined) {
    wsio.close();
  }
};

/**
 * Idle function, show and hide the UI, triggered at uiTimerDelay sec delay
 *
 * @method resetIdle
 */
function resetIdle() {
  if (uiTimer) {
    clearTimeout(uiTimer);
    ui.showInterface();
    uiTimer = setTimeout(function() { ui.hideInterface(); }, uiTimerDelay * 1000);
  }
}

function isLandscape(){
  if(screen.width > screen.height) {
    return true;
  } 
  return false;
}

function calculateAppSize(sageWidth, sageHeight){
  var data = {};
  var appRatio = sageWidth/sageHeight;
  var mobileRatio = screen.width/screen.height;
  // var quality = document.getElementById("qualitySelect").value;
  var quality = 1;

  data.width = sageWidth;
  data.height = sageHeight;

    if (appRatio < mobileRatio){
    data.scale = (screen.height - buttonHeight) / sageHeight;
  }else{
    data.scale = screen.width/sageWidth;
  }

  var appID = document.getElementById("runningAppSelect").value;
  if ((getAppName(appID)).substring(0,16) == "Sketchfab Viewer"){
    console.log("Sketchfab Browser running...performance enhancement recalculation");
    var sageItems = document.getElementsByClassName("sageItem");
    for (var i =0; i< sageItems.length; i++){
      if (sageItems[i].parentElement.style.zIndex == "1"){
        if (appRatio < mobileRatio){
          sageHeight = (screen.height - buttonHeight)/quality;
          sageWidth = sageHeight;
          data.width = sageWidth;
          data.height = sageHeight;
        }else{
          sageWidth = screen.width/quality;
          sageHeight = sageWidth;
          data.width = sageWidth;
          data.height = sageHeight;
        }        
        sageItems[i].style.width = sageWidth + "px";
        sageItems[i].style.height = sageHeight + "px";
        break;
      }
    }
  }

  // if (appRatio < mobileRatio){
  //   data.scale = (screen.height - buttonHeight) / sageHeight;
  // }else{
  //   data.scale = screen.width/sageWidth;
  // }

  data.top = buttonHeight;
  data.left = 0;

  // data.width = sageWidth;
  // data.height = sageHeight;
  data.top = buttonHeight;
  data.left = 0;
  scale = data.scale;

  return data;
}

function scaleApp(){

}

function handleOrientationChange(){
  if (screen.width == screen.height)return;
  
  var appID = document.getElementById("runningAppSelect").value;

  updateAppSize(appID);

}

function updateAppSize(appID){
  var appID = document.getElementById("runningAppSelect").value;
  var touch =  document.getElementById("touchLayer");
  var appWindows = document.getElementById("appWindows");
  var appState = document.getElementById(appID+"_state");  
  var appData = calculateAppSize(parseInt(appState.style.width,10), parseInt(appState.style.height,10));

  // touch.style.width = appState.style.width;
  // touch.style.height = appState.style.height;
  touch.style.width = appData.width + "px";
  touch.style.height = appData.height + "px";
  console.log("updateAppSize");
  // performance enhancement

  if ((getAppName(appID)).substring(0,16) == "Sketchfab Viewer"){
    var transform = "scale("+1+")";
    appWindows.style.webkitTransform = transform;
    appWindows.style.mozTransform    = transform;
    appWindows.style.transform       = transform;
  }else{
    var transform = "scale("+appData.scale+")";
    appWindows.style.webkitTransform = transform;
    appWindows.style.mozTransform    = transform;
    appWindows.style.transform       = transform;
  }
}

function handlePinchin(event){
  handleScroll(0, event);
}

function handlePinchout(event){
  handleScroll(1, event);
}

function startMobilePointer(){
  var pointerData = {};
  pointerData.sourceType = undefined;
  pointerData.label = ""

  wsio.emit('stopSagePointer'); 
  wsio.emit('startSagePointer', pointerData);

  var data = {};
  setTimeout(function(){
    data.id = wsio.id;
    data.code = 16;
    wsio.emit('keyDown', data);
    data.code = 9;
    wsio.emit('keyPress', data);
    data.code = 16;
    wsio.emit('keyUp', data);
  }, 100);
}
  


function handleScroll(type, event){

  var appPos = applications[document.getElementById("runningAppSelect").value];
  var touchL = document.getElementById("touchLayer");

  // var appData = calculateAppSize(appPos.sage2_width,appPos.sage2_height);
  var appData = {};
  appData.scale = scale;
  var sageOffsetX = event.center.x / appData.scale;
  var sageOffsetY = ((event.center.y - buttonHeight) / appData.scale);
  sageOffsetY += 108;

  var dataMove = {};
  dataMove.dx = (appPos.sage2_x + sageOffsetX) - pointerPos.left;
  dataMove.dy = (appPos.sage2_y + sageOffsetY) - pointerPos.top;
  pointerPos.left = appPos.sage2_x + sageOffsetX;
  pointerPos.top = appPos.sage2_y + sageOffsetY;
  
  
  var dataScroll = {};
  var delta = 5;
  if (type == 0){
    dataScroll.wheelDelta = delta;    
  }else{
    dataScroll.wheelDelta = -delta;
  }


  wsio.emit('pointerScrollStart');

  wsio.emit('pointerMove', dataMove);
  wsio.emit('pointerScroll', dataScroll);
  
  wsio.emit('pointerScrollEnd');
}

// deprecated
// function checkBorder(direct){
//   var appPos = applications[document.getElementById("runningAppSelect").value];
//   if (direct == "l"){
//     if ((pointerPos.left - pointerSpeed) <= appPos.sage2_x) return true;
//   }else if(direct == "r"){
//     if ((pointerPos.left + pointerSpeed) >= (appPos.sage2_x + appPos.sage2_width)) return true;
//   }else if(direct == "u"){
//     if ((pointerPos.top - pointerSpeed - 108) <= appPos.sage2_y) return true;
//   }else if(direct == "d") {
//     if ((pointerPos.top + pointerSpeed - 108) >= (appPos.sage2_y + appPos.sage2_height)) return true;
//   }
//   return false;
// }

// preventing pointer to slide off of selected app window
function checkBorder(dx, dy){
  var appPos = applications[document.getElementById("runningAppSelect").value];
  //left
  if (dx<0){
    if ((pointerPos.left + dx) <= appPos.sage2_x) return true;
  }
  //right
  if(dx>0){
    if ((pointerPos.left + dx) >= (appPos.sage2_x + appPos.sage2_width)) return true;
  }
  //up
  if(dy<0){
    if ((pointerPos.top + dy - 108) <= appPos.sage2_y) return true;
  }
  //down
  if(dy>0) {
    if ((pointerPos.top + dy - 108) >= (appPos.sage2_y + appPos.sage2_height)) return true;
  }
  return false;
}


function handleRelease(event){
  var dataPress = {};
  dataPress.button = "left";
  wsio.emit('pointerRelease', dataPress);
  pressed = false;
}

function handlePan(event){
  var newMove = +new Date();

  if(newMove - lastMove < 30){
    // console.log("notMoving");
    return;
  }else{
    lastMove = newMove;
  }
  
  if (!pressed){
    handlePress(event);
  }

  var appPos = applications[document.getElementById("runningAppSelect").value];
  // var appData = calculateAppSize(appPos.sage2_width,appPos.sage2_height);
  
  var dx = event.deltaX / 15;
  var dy = event.deltaY / 15;
  
  if (checkBorder(dx, dy))return;

  var dataMove = {};
  dataMove.dx = dx;
  dataMove.dy = dy;
  pointerPos.left += dx;
  pointerPos.top += dy;
  wsio.emit('pointerMove', dataMove);
}

function handlePanStop (event){
  var dataPress = {};
  dataPress.button = "left";
  wsio.emit('pointerRelease', dataPress);
  pressed = false;
}

// deprecated
// function handlePanLeft(event){
//   if (checkBorder("l")){
//     return;
//   }
//   var dataMove = {};
//   dataMove.dx = -pointerSpeed;
//   pointerPos.left += -pointerSpeed;
//   wsio.emit('pointerMove', dataMove);
// }

// function handlePanRight(event){
//   if (checkBorder("r")){
//     return;
//   }
//   var dataMove = {};
//   dataMove.dx = pointerSpeed;
//   pointerPos.left += pointerSpeed;
//   wsio.emit('pointerMove', dataMove);
// }

// function handlePanUp(event){
//   if (checkBorder("u")){
//     return;
//   }
//   var dataMove = {};
//   dataMove.dy = -pointerSpeed;
//   pointerPos.top += -pointerSpeed;
//   wsio.emit('pointerMove', dataMove);
// }

// function handlePanDown(event){
//   if (checkBorder("d")){
//     return;
//   }
//   var dataMove = {};
//   dataMove.dy = pointerSpeed;
//    pointerPos.top += pointerSpeed;
//   wsio.emit('pointerMove', dataMove);
// }


function handlePress(event){
  if (pressed)return;
  var appID = document.getElementById("runningAppSelect").value;
  var appPos = applications[appID];
  // var touchL = document.getElementById("touchLayer");

  // var appData = calculateAppSize(appPos.sage2_width,appPos.sage2_height);
  var appData = {};
  appData.scale = scale;

  var sageOffsetX = event.center.x / appData.scale;
    
  var appState = document.getElementById(appID+"_state");
  
  var sageOffsetY = ((event.center.y - buttonHeight) / appData.scale);
  sageOffsetY += 108;

  var dataMove = {};
  dataMove.dx = (appPos.sage2_x + sageOffsetX) - pointerPos.left;
  dataMove.dy = (appPos.sage2_y + sageOffsetY) - pointerPos.top;
  pointerPos.left = appPos.sage2_x + sageOffsetX;
  pointerPos.top = appPos.sage2_y + sageOffsetY;


  wsio.emit('pointerMove', dataMove);

  var dataPress = {};
  dataPress.button = "left";

  // console.log(+new Date());
  wsio.emit('pointerPress', dataPress); 
  pressed = true;
  
}

function handlePanStart(event){

}

function addTouchLayer(){
  var touchLayer = document.getElementById("touchLayer");
  touchLayer.id = "touchLayer";
  touchLayer.style.position = "absolute";
  touchLayer.style.top = "0px";
  touchLayer.style.left = "0px";
  touchLayer.style.zIndex = 0;

  // touch gesture recognition
  var pinch = new Hammer.Pinch();
  var pan = new Hammer.Pan({ direction: Hammer.DIRECTION_ALL });
  var press = new Hammer.Press({time: 0 , threshold: 20});
  var tap = new Hammer.Tap({pointers: 2, taps: 1});
  hammertime = new Hammer(touchLayer);
  
  tap.recognizeWith(press);
  pan.recognizeWith(press);

  mc = new Hammer.Manager(touchLayer);
  mc.add(pinch);
  mc.add(press);
  mc.add(pan);
  mc.on("pinchin", handlePinchin);
  mc.on("pinchout", handlePinchout);
  // deprecated
  // mc.on("panleft", handlePanLeft);
  // mc.on("panright", handlePanRight);
  // mc.on("panup", handlePanUp);
  // mc.on("pandown", handlePanDown);
  mc.on("press", handlePress);
  mc.on("panstart", handlePanStart);
  mc.on("pressup", handleRelease);
  mc.on("panend", handlePanStop);
  mc.on("pancancel", handlePanStop);
  mc.on("pan", handlePan);

  document.body.style.overflow = 'hidden';
}

/**
 * Entry point of the application
 *
 * @method SAGE2_init
 */
function Mobile_init() {

  document.addEventListener("keydown", handleTest);
  document.getElementById("topBar").style.position = "fixed";
  document.getElementById("topBar").style.top = "0px";
  document.getElementById("topBar").style.left = "0px";
  document.getElementById("runningAppSelect").addEventListener("change", appSelected);
  // document.getElementById("background").style.display = "none";
  document.getElementById("background").style.visibility = "hidden";
  window.addEventListener("orientationchange", handleOrientationChange);
  addTouchLayer();

  document.getElementById("appWindows").style.position = "fixed";
  document.getElementById("appWindows").style.top = buttonHeight + "px";
  document.getElementById("appWindows").style.left = "0px";

  clientID = parseInt(getParameterByName("clientID")) || 0;
  console.log("clientID: " + clientID);

  wsio = new WebsocketIO();
  console.log("Connected to server: ", window.location.origin);
  console.log(wsio.id);

  // Detect the current browser
  SAGE2_browser();

  isMaster = false;

  wsio.open(function() {
    console.log("Websocket opened");

    setupListeners();

    var clientDescription = {
      //clientType: "display",
      clientType: "sageUI",
      clientID: clientID,
      requests: {
        config: true,
        version: true,
        time: true,
        console: false
      }
    };
    wsio.emit('addClient', clientDescription);
  //});

     clientDescription = {
      clientType: "display",
      //clientType: "sageUI",
      clientID: clientID,
      requests: {
        config: true,
        version: true,
        time: true,
        console: false
      }
    };
    wsio.emit('addClient', clientDescription);

  });


  // Socket close event (ie server crashed)
  wsio.on('close', function(evt) {
    var refresh = setInterval(function() {
      // make a dummy request to test the server every 2 sec
      var xhr = new XMLHttpRequest();
      xhr.open("GET", "/", true);
      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
          console.log("server ready");
          // when server ready, clear the interval callback
          clearInterval(refresh);
          // and reload the page
          window.location.reload();
        }
      };
      xhr.send();
    }, 2000);
  });
 


  setTimeout(startMobilePointer, 100);
  setTimeout(appSelected, 100);
}

function onExit(){
  wsio.emit('stopSagePointer');
}

function resetPointer(){
  var pointerData = {};
  pointerData.sourceType = undefined;

  wsio.emit('stopSagePointer');
  wsio.emit('startSagePointer', pointerData);
  var data = {};
  data.id = wsio.id;
  data.code = 16;
  wsio.emit('keyDown', data);
  data.code = 9;
  wsio.emit('keyPress', data);
  data.code = 16;
  wsio.emit('keyUp', data);

}

// function toggleWidget(){
// // TODO
// }


function requestFullScreen(){  
  var el = document.body;

  if (!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement &&
    !document.msFullscreenElement ) {
    // turn fullscreen on    
    var requestMethod = el.requestFullScreen || el.webkitRequestFullscreen 
    || el.mozRequestFullScreen || el.msRequestFullScreen;

    if (requestMethod) {
      requestMethod.call(el);
    } else if (typeof window.ActiveXObject !== "undefined") {
      // Older IE.
      var wscript = new ActiveXObject("WScript.Shell");

      if (wscript !== null) {
        wscript.SendKeys("{F11}");
      }
    }
    
  }else{
    // turn fullscreen off
    if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if (document.msExitFullscreen) {
      document.msExitFullscreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
      document.webkitExitFullscreen();
    }

  }
}


function appSelected(){
   var appID = document.getElementById("runningAppSelect").value;
   reorderZ(appID);
   updateAppSize(appID);
}

function reorderZ(selected){
  var runningApps = document.getElementsByClassName("windowItem");
  var index;
  for (index = 0; index < runningApps.length; index++ ) {
    runningApps[index].style.zIndex = "0";
    runningApps[index].style.visibility='hidden';
  }
  document.getElementById(selected).style.zIndex = "1";
  document.getElementById(selected).style.visibility='visible';
}

function updateSelectBox(){
  var index;
  var name;
  var select = document.getElementById("runningAppSelect");  
  var runningApps = document.getElementsByClassName("windowItem");
  
  var length = select.options.length;
 
  for (index = 0; index < length; index++) {
    select.options[index] = null;
  }
  
  length = runningApps.length;
  console.log(runningApps);
  for(index = 0; index < length; index++) {
    name = document.getElementById(runningApps[index].id+"_text").innerHTML;
    select.options[index] = new Option(name, runningApps[index].id);
  }
}

function handleTest(){
  // debugging tests here
}



function resizeTouchLayer(appID){
  var touch =  document.getElementById("touchLayer");
  var app  = document.getElementById("div" + appID);
  var appTitle = document.getElementById(appID);

  touch.style.height = app.style.height;
  touch.style.width = app.style.width;

  console.log(appTitle);

  var translate = "translate(" + appTitle.style.left + "px," + appTitle.style.top + "px)";

  touch.style.webkitTransform = translate;
  touch.style.mozTransform    = translate;
  touch.style.transform       = translate;

}

function getAppName(appID){
  return document.getElementById(appID + "_text").innerHTML;
}


function setupListeners() {

  wsio.on("appPositionAndSize", function(appData){
    console.log("ReceivedAppData of application " + appData.id);
  });

  wsio.on('availableApplications', function(data) {
      console.log(data.length);

  });

  wsio.on('initialize', function(data) {
    var startTime  = new Date(data.start);

    // Global initialization
    SAGE2_initialize(startTime);
  });

  wsio.on('setAsMasterDisplay', function() {
    isMaster = true;
  });

  wsio.on('broadcast', function(data) {
    if (applications[data.app] === undefined) {
      // should have better way to determine if app is loaded
      //   or already killed
      setTimeout(function() {
        if (applications[data.app] && applications[data.app][data.func]) {
          applications[data.app][data.func](data.data);
        }
      }, 500);
    } else {
      applications[data.app][data.func](data.data);
    }
  });

  wsio.on('addScript', function(script_data) {
    var js = document.createElement('script');
    js.type = "text/javascript";
    js.src = script_data.source;
    document.head.appendChild(js);
  });

  wsio.on('setupDisplayConfiguration', function(json_cfg) {
    var i;
    var http_port;
    var https_port;

    http_port = json_cfg.index_port === 80 ? "" : ":" + json_cfg.index_port;
    https_port = json_cfg.port === 443 ? "" : ":" + json_cfg.port;
    hostAlias["http://"  + json_cfg.host + http_port]  = window.location.origin;
    hostAlias["https://" + json_cfg.host + https_port] = window.location.origin;
    for (i = 0; i < json_cfg.alternate_hosts.length; i++) {
      hostAlias["http://"  + json_cfg.alternate_hosts[i] + http_port]  = window.location.origin;
      hostAlias["https://" + json_cfg.alternate_hosts[i] + https_port] = window.location.origin;
    }

    // Build the elements visible on the wall
    ui = new UIBuilder(json_cfg, clientID);
    ui.build();
    //ui.background();
    if (json_cfg.ui.auto_hide_ui) {
      // default delay is 30s if not specified
      uiTimerDelay = json_cfg.ui.auto_hide_delay ? parseInt(json_cfg.ui.auto_hide_delay, 10) : 30;
      uiTimer      = setTimeout(function() { ui.hideInterface(); }, uiTimerDelay * 1000);
    }
    makeSvgBackgroundForWidgetConnectors(ui.main.style.width, ui.main.style.height);
    ui.hideInterface();
  });

  wsio.on('setupSAGE2Version', function(version) {
    ui.updateVersionText(version);
  });

  wsio.on('setSystemTime', function(data) {
    ui.setTime(new Date(data.date));
  });

  wsio.on('addRemoteSite', function(data) {
    ui.addRemoteSite(data);
  });

  wsio.on('connectedToRemoteSite', function(data) {
    if (window.ui) {
      ui.connectedToRemoteSite(data);
    } else {
      setTimeout(function() {
        ui.connectedToRemoteSite(data);
      }, 1000);
    }
  });

  wsio.on('createSagePointer', function(pointer_data) {
    if (window.ui) {
      // kubisjir
      // ui.createSagePointer(pointer_data);
    } else {
      setTimeout(function() {
        ui.createSagePointer(pointer_data);
      }, 1000);
    }
  });

  // wsio.on('updateSagePointerPosition', function(pointer_data) {
  //   ui.updateSagePointerPosition(pointer_data);
  //   resetIdle();
  // });

  // wsio.on('changeSagePointerMode', function(pointer_data) {
  //   ui.changeSagePointerMode(pointer_data);
  //   resetIdle();
  // });

  wsio.on('createRadialMenu', function(menu_data) {
    ui.createRadialMenu(menu_data);
  });

  wsio.on('updateRadialMenu', function(menu_data) {
    ui.updateRadialMenu(menu_data);
  });

  wsio.on('radialMenuEvent', function(menu_data) {
    ui.radialMenuEvent(menu_data);
    resetIdle();
  });

  wsio.on('updateRadialMenuDocs', function(menu_data) {
    ui.updateRadialMenuDocs(menu_data);
    resetIdle();
  });

  wsio.on('updateRadialMenuApps', function(menu_data) {
    ui.updateRadialMenuApps(menu_data);
    resetIdle();
  });

  wsio.on('loadApplicationState', function(data) {
    var app = applications[data.id];
    if (app !== undefined && app !== null) {
      app.SAGE2Load(data.state, new Date(data.date));
    }
  });

  wsio.on('updateMediaStreamFrame', function(data) {
    wsio.emit('receivedMediaStreamFrame', {id: data.id});

    var app = applications[data.id];
    if (app !== undefined && app !== null) {
      app.SAGE2Load(data.state, new Date(data.date));
    }

    // update clones in data-sharing portals
    var key;
    for (key in dataSharingPortals) {
      app = applications[data.id + "_" + key];
      if (app !== undefined && app !== null) {
        app.SAGE2Load(data.state, new Date(data.date));
      }
    }
  });

  wsio.on('updateMediaBlockStreamFrame', function(data) {
    var appId     = byteBufferToString(data);
    var blockIdx  = byteBufferToInt(data.subarray(appId.length + 1, appId.length + 3));
    var date      = byteBufferToInt(data.subarray(appId.length + 3, appId.length + 11));
    var yuvBuffer = data.subarray(appId.length + 11, data.length);

    if (applications[appId] !== undefined && applications[appId] !== null) {
      applications[appId].textureData(blockIdx, yuvBuffer);
      if (applications[appId].receivedBlocks.every(isTrue) === true) {
        applications[appId].refresh(new Date(date));
        applications[appId].setValidBlocksFalse();
        wsio.emit('receivedMediaBlockStreamFrame', {id: appId});
      }
    }
  });

  wsio.on('updateVideoFrame', function(data) {
    var appId     = byteBufferToString(data);
    var blockIdx  = byteBufferToInt(data.subarray(appId.length + 1, appId.length + 3));
    var date      = byteBufferToInt(data.subarray(appId.length + 7, appId.length + 15));
    var yuvBuffer = data.subarray(appId.length + 15, data.length);

    if (applications[appId] !== undefined && applications[appId] !== null) {
      applications[appId].textureData(blockIdx, yuvBuffer);
      if (applications[appId].receivedBlocks.every(isTrue) === true) {
        applications[appId].refresh(new Date(date));
        applications[appId].setValidBlocksFalse();
        wsio.emit('requestVideoFrame', {id: appId});
      }
    }
  });

  wsio.on('updateFrameIndex', function(data) {
    var app = applications[data.id];
    if (app !== undefined && app !== null) {
      app.setVideoFrame(data.frameIdx);
    }
  });

  wsio.on('videoEnded', function(data) {
    var app = applications[data.id];
    if (app !== undefined && app !== null) {
      app.videoEnded();
    }
  });

  wsio.on('updateValidStreamBlocks', function(data) {
    if (applications[data.id] !== undefined && applications[data.id] !== null) {
      applications[data.id].validBlocks = data.blockList;
      applications[data.id].setValidBlocksFalse();
    }
  });

  wsio.on('updateWebpageStreamFrame', function(data) {
    wsio.emit('receivedWebpageStreamFrame', {id: data.id, client: clientID});

    var webpage = document.getElementById(data.id + "_webpage");
    webpage.src = "data:image/jpeg;base64," + data.src;
  });

  wsio.on('createAppWindow', function(data) {
    ui.titleBarHeight = buttonHeight;
    ui.titleTextHeight = buttonHeight;
    createAppWindow(data, ui.main.id, ui.titleBarHeight, ui.titleTextSize, ui.offsetX, ui.offsetY);
  });

  wsio.on('createAppWindowInDataSharingPortal', function(data) {
    var portal = dataSharingPortals[data.portal];

    createAppWindow(data.application, portal.id, portal.titleBarHeight, portal.titleTextSize, 0, 0);
  });

  wsio.on('deleteElement', function(elem_data) {
    resetIdle();

    // Tell the application it is over
    var app = applications[elem_data.elemId];
    app.terminate();
    // Remove the app from the list
    delete applications[elem_data.elemId];

    // Clean up the DOM
    // var deleteElemTitle = document.getElementById(elem_data.elemId + "_title");
    // deleteElemTitle.parentNode.removeChild(deleteElemTitle);

    var deleteElem = document.getElementById(elem_data.elemId);
    deleteElem.parentNode.removeChild(deleteElem);

    // Clean up the UI DOM
    if (elem_data.elemId in controlObjects) {
      for (var item in controlItems) {
        if (item.indexOf(elem_data.elemId) > -1) {
          controlItems[item].divHandle.parentNode.removeChild(controlItems[item].divHandle);
          removeWidgetToAppConnector(item);
          delete controlItems[item];
        }

      }
      delete controlObjects[elem_data.elemId];
    }
    updateSelectBox();
  });

  wsio.on('hideControl', function(ctrl_data) {
    if (ctrl_data.id in controlItems && controlItems[ctrl_data.id].show === true) {
      controlItems[ctrl_data.id].divHandle.style.display = "none";
      controlItems[ctrl_data.id].show = false;
      clearConnectorColor(ctrl_data.id, ctrl_data.appId);
    }
  });

  wsio.on('showControl', function(ctrl_data) {
    if (ctrl_data.id in controlItems && controlItems[ctrl_data.id].show === false) {
      controlItems[ctrl_data.id].divHandle.style.display = "block";
      controlItems[ctrl_data.id].show = true;
    }
  });

  wsio.on('updateItemOrder', function(order) {
    resetIdle();
  });

  wsio.on('setItemPosition', function(position_data) {
    resetIdle();

    if (position_data.elemId.split("_")[0] === "portal") {
      dataSharingPortals[position_data.elemId].setPosition(position_data.elemLeft, position_data.elemTop);
      return;
    }

    var translate = "translate(" + position_data.elemLeft + "px," + position_data.elemTop + "px)";
    var selectedElemTitle = document.getElementById(position_data.elemId + "_title");
  //   selectedElemTitle.style.webkitTransform = translate;
  //   selectedElemTitle.style.mozTransform    = translate;
  //   selectedElemTitle.style.transform       = translate;

    var selectedElem = document.getElementById(position_data.elemId);
  //   selectedElem.style.webkitTransform = translate;
  //   selectedElem.style.mozTransform    = translate;
  //   selectedElem.style.transform       = translate;

    var app = applications[position_data.elemId];
    if (app !== undefined) {
      var parentTransform = getTransform(selectedElem.parentNode);
      var border = parseInt(selectedElem.parentNode.style.borderWidth || 0, 10);
      // app.sage2_x = (position_data.elemLeft + border + 1) * parentTransform.scale.x + parentTransform.translate.x;
      // app.sage2_y = (position_data.elemTop + ui.titleBarHeight + border) * parentTransform.scale.y + parentTransform.translate.y;
      app.sage2_x = (position_data.elemLeft+1);
      app.sage2_y = (position_data.elemTop);
      // app.sage2_width  = parseInt(position_data.elemWidth, 10) * parentTransform.scale.x;
      // app.sage2_height = parseInt(position_data.elemHeight, 10) * parentTransform.scale.y;
      app.sage2_width  = parseInt(position_data.elemWidth, 10);
      app.sage2_height = parseInt(position_data.elemHeight, 10);
      var date  = new Date(position_data.date);
      if (position_data.force || app.moveEvents === "continuous") {
        app.move(date);
      }
    }
    if (position_data.elemId in controlObjects) {
      var hOffset = (ui.titleBarHeight + position_data.elemHeight) / 2;
      for (var item in controlItems) {
        if (controlItems.hasOwnProperty(item) && item.indexOf(position_data.elemId) > -1 && controlItems[item].show) {
          var control = controlItems[item].divHandle;
          var cLeft = parseInt(control.style.left);
          var cTop = parseInt(control.style.top);
          var cHeight = parseInt(control.style.height);
          moveWidgetToAppConnector(item, cLeft + cHeight / 2.0, cTop + cHeight / 2.0,
            position_data.elemLeft - ui.offsetX + position_data.elemWidth / 2.0,
            position_data.elemTop - ui.offsetY + hOffset, cHeight / 2.4);
        }
      }
    }

    handleOrientationChange();

  });

  wsio.on('setControlPosition', function(position_data) {
    var eLeft = position_data.elemLeft - ui.offsetX;
    var eTop = position_data.elemTop - ui.offsetY;
    var selectedControl = document.getElementById(position_data.elemId);
    var appData = position_data.appData;
    if (selectedControl !== undefined && selectedControl !== null) {
      selectedControl.style.left = eLeft.toString() + "px";
      selectedControl.style.top = eTop.toString() + "px";
      var hOffset = (ui.titleBarHeight + appData.height) / 2;
      moveWidgetToAppConnector(position_data.elemId,
        eLeft + position_data.elemHeight / 2.0,
        eTop + position_data.elemHeight / 2.0,
        appData.left - ui.offsetX + appData.width / 2.0,
        appData.top - ui.offsetY + hOffset,
        position_data.elemHeight / 2.4);
    } else {
      console.log("cannot find control: " + position_data.elemId);
    }
  });

  wsio.on('showWidgetToAppConnector', function(data) {
    showWidgetToAppConnectors(data);
    if (data.user_color !== null) {
      if (!(data.id in widgetConnectorRequestList)) {
        widgetConnectorRequestList[data.id] = [];
      }
      widgetConnectorRequestList[data.id].push(data);
    }
  });


  wsio.on('hideWidgetToAppConnector', function(control_data) {
    if (control_data.id in widgetConnectorRequestList) {
      var lst = widgetConnectorRequestList[control_data.id];
      if (lst.length > 1) {
        var len = lst.length;
        for (var i = len - 1; i >= 0; i--) {
          if (control_data.user_id === lst[i].user_id) {
            lst.splice(i, 1);
            showWidgetToAppConnectors(lst[len - 2]);
            break;
          }
        }
      } else if (lst.length === 1) {
        delete widgetConnectorRequestList[control_data.id];
        hideWidgetToAppConnectors(control_data.id);
      }
    }

  });

  wsio.on('setItemPositionAndSize', function(position_data) {
    resetIdle();

    if (position_data.elemId.split("_")[0] === "portal") {
      dataSharingPortals[position_data.elemId].setPositionAndSize(position_data.elemLeft,
          position_data.elemTop, position_data.elemWidth, position_data.elemHeight);
      return;
    }
    var selectedElem = document.getElementById(position_data.elemId);
    var child        = selectedElem.getElementsByClassName("sageItem");
    // If application not ready, return
    if (child.length < 1) {
      return;
    }

    var selectedElemState = document.getElementById(position_data.elemId + "_state");
    selectedElemState.style.width = Math.round(position_data.elemWidth).toString() + "px";
    selectedElemState.style.height = Math.round(position_data.elemHeight).toString() + "px";

    // if the element is a div or iframe, resize should use the style object
    if (child[0].tagName.toLowerCase() === "div" || child[0].tagName.toLowerCase() === "iframe") {
      child[0].style.width  = Math.round(position_data.elemWidth)  + "px";
      child[0].style.height = Math.round(position_data.elemHeight) + "px";
    } else {
      // if it's a canvas or else, just use width and height
      child[0].width  = Math.round(position_data.elemWidth);
      child[0].height = Math.round(position_data.elemHeight);
    }

    var app = applications[position_data.elemId];
    if (app !== undefined) {
      var parentTransform = getTransform(selectedElem.parentNode);
      var border = parseInt(selectedElem.parentNode.style.borderWidth || 0, 10);
      // app.sage2_x = (position_data.elemLeft + border + 1) * parentTransform.scale.x + parentTransform.translate.x;
      app.sage2_x = (position_data.elemLeft + 1);
      app.sage2_x = Math.round(app.sage2_x);
      // app.sage2_y = (position_data.elemTop + ui.titleBarHeight + border) * parentTransform.scale.y + parentTransform.translate.y;
      app.sage2_y = (position_data.elemTop);
      app.sage2_y = Math.round(app.sage2_y);
      // app.sage2_width  = parseInt(position_data.elemWidth, 10) * parentTransform.scale.x;
      // app.sage2_height = parseInt(position_data.elemHeight, 10) * parentTransform.scale.y;
      app.sage2_width  = parseInt(position_data.elemWidth, 10);
      app.sage2_height = parseInt(position_data.elemHeight, 10);

      var date = new Date(position_data.date);
      if (position_data.force || app.resizeEvents === "continuous") {
        if (app.resize) {
          app.resize(date);
        }
      }
      if (position_data.force || app.moveEvents === "continuous") {
        if (app.move) {
          app.move(date);
        }
      }
    }
    if (position_data.elemId in controlObjects) {
      var hOffset = (ui.titleBarHeight + position_data.elemHeight) / 2;
      for (var item in controlItems) {
        if (controlItems.hasOwnProperty(item) && item.indexOf(position_data.elemId) > -1 && controlItems[item].show) {
          var control = controlItems[item].divHandle;
          var cLeft = parseInt(control.style.left);
          var cTop = parseInt(control.style.top);
          var cHeight = parseInt(control.style.height);
          moveWidgetToAppConnector(item, cLeft + cHeight / 2.0,
            cTop + cHeight / 2.0,
            position_data.elemLeft - ui.offsetX + position_data.elemWidth / 2.0,
            position_data.elemTop - ui.offsetY + hOffset,
            cHeight / 2.4);
        }
      }
    }
    
    handleOrientationChange();
  });

  wsio.on('startMove', function(data) {
    resetIdle();

    var app = applications[data.id];
    if (app !== undefined && app.moveEvents === "onfinish") {
      var date = new Date(data.date);
      if (app.startMove) {
        app.startMove(date);
      }
    }
  });

  wsio.on('finishedMove', function(data) {
    resetIdle();

    var app = applications[data.id];
    if (app !== undefined && app.moveEvents === "onfinish") {
      var date = new Date(data.date);
      if (app.move) {
        app.move(date);
      }
    }
  });

  wsio.on('startResize', function(data) {
    resetIdle();

    var app = applications[data.id];
    if (app !== undefined && app.resizeEvents === "onfinish") {
      var date = new Date(data.date);
      if (app.startResize) {
        app.startResize(date);
      }
    }
  });

  wsio.on('finishedResize', function(data) {
    resetIdle();
    var app = applications[data.id];
    if (app !== undefined && app.resizeEvents === "onfinish") {
      var date = new Date(data.date);
      if (app.resize) {
        app.resize(date);
      }
    }
  });

  //kubisjir ?
  wsio.on('animateCanvas', function(data) {
    var app = applications[data.id];
    if (app !== undefined && app !== null) {
      var date = new Date(data.date);
      app.refresh(date);
      wsio.emit('finishedRenderingAppFrame', {id: data.id, fps: app.maxFPS});
    }
  });

  wsio.on('eventInItem', function(event_data) {
    var date = new Date(event_data.date);
    var app  = applications[event_data.id];

    app.SAGE2Event(event_data.type, event_data.position, event_data.user, event_data.data, date);
  });

  wsio.on('requestNewControl', function(data) {
    var dt = new Date(data.date);
    if (data.elemId !== undefined && data.elemId !== null) {
      if (controlObjects[data.elemId] !== undefined) {

        var spec = controlObjects[data.elemId].controls;
        if (spec.controlsReady() === true) {
          var size = spec.computeSize();
          wsio.emit('addNewControl', {
            id: data.elemId + data.user_id + "_controls",
            appId: data.elemId,
            left: data.x - size.height / 2,
            top: data.y - size.height / 2,
            width: size.width,
            height: size.height,
            barHeight: size.barHeight,
            hasSideBar: size.hasSideBar,
            show: true,
            date: dt
          });
        }

      }
    }
  });

  wsio.on('createControl', function(data) {
    if (controlItems[data.id] === null || controlItems[data.id] === undefined) {
      var ctrDiv =  document.createElement("div");
      ctrDiv.id = data.id;
      ctrDiv.className = "windowControls";
      ctrDiv.style.width = data.width.toString() + "px";
      ctrDiv.style.fill = "rgba(0,0,0,0.0)";
      ctrDiv.style.height = data.height.toString() + "px";
      ctrDiv.style.left = (data.left - ui.offsetX).toString() + "px";
      ctrDiv.style.top = (data.top - ui.offsetY).toString() + "px";
      ctrDiv.style.zIndex = "9990".toString();
      ctrDiv.style.display = data.show ? "block" : "none";
      if (ui.noDropShadow === true) {
        ctrDiv.style.boxShadow = "none";
      }

      var spec = controlObjects[data.appId].controls;
      if (spec.controlsReady() === true) {
        var handle = new SAGE2WidgetControlInstance(data.id, spec);
        ctrDiv.appendChild(handle);
        ui.main.appendChild(ctrDiv);
        controlItems[data.id] = {show: data.show, divHandle: ctrDiv};
        createWidgetToAppConnector(data.id);
      }

    }
  });
  wsio.on('removeControlsForUser', function(data) {
    for (var idx in controlItems) {
      if (idx.indexOf(data.user_id) > -1) {
        controlItems[idx].divHandle.parentNode.removeChild(controlItems[idx].divHandle);
        removeWidgetToAppConnector(idx);
        delete controlItems[idx];
      }
    }
  });


wsio.on('executeControlFunction', function(data) {
    console.log(data);
    var ctrl = getWidgetControlInstanceById(data.ctrl);
    if (ctrl) {
      var ctrlId = ctrl.attr('id');
      var action = "buttonPress";
      var ctrlParent = ctrl.parent();
      if (/button/.test(ctrlId)) {
        ctrl = ctrlParent.select("svg");
        var animationInfo = ctrlParent.data("animationInfo");
        var state = animationInfo.state;
        if (ctrl !== null && ctrl !== undefined) {
          if (state === null || state === undefined) {
            ctrl = ctrlParent.select("circle") || ctrlParent.select("polygon");
            if (ctrl !== null && ctrl !== undefined) {
              var fillVal = ctrl.attr("fill");
              ctrl.animate({fill: "rgba(230,230,230,1.0)"}, 400, mina.bounce, function() {
                ctrl.animate({fill: fillVal}, 400, mina.bounce);
              });
            }
          }/* else if (state === 1) {
            ctrlParent.select("#cover2").attr("visibility", "visible");
            ctrlParent.select("#cover").attr("visibility", "hidden");
            animationInfo.state = 0;
          } else {
            ctrlParent.select("#cover").attr("visibility", "visible");
            ctrlParent.select("#cover2").attr("visibility", "hidden");
            animationInfo.state = 1;
          }*/
        } else {
          console.log(ctrl);
          ctrl = ctrlParent.select("path") || ctrlParent.select("text");
          if (animationInfo.textual === false && animationInfo.animation === true) {
            var delay = animationInfo.delay;
            var fromPath = animationInfo.from;
            var toPath = animationInfo.to;
            var fromFill = animationInfo.fill;
            var toFill = animationInfo.toFill;
            if (toFill === null || toFill === undefined) {
              toFill = fromFill;
            }
            if (state === null) {
              ctrl.animate({path: toPath, fill: toFill}, delay, mina.bounce, function() {
                ctrl.animate({path: fromPath, fill: fromFill}, delay, mina.bounce);
              });

            } else {
              animationInfo.state = 1 - animationInfo.state;
              ctrl.data("animationInfo", animationInfo);
              // ctrl.animate({"path":path, "fill":fill}, delay, mina.bounce);
            }
          }
        }
        ctrlId = ctrlParent.attr("id").replace("button", "");
      } else {
        ctrlId = ctrlParent.attr("id").replace("slider", "");
        action = "sliderRelease";
      }

      var appId = data.ctrl.appId;
      var app   = applications[appId];
      switch (ctrlId) {
        case "CloseApp":
          if (isMaster) {
            wsio.emit('closeAppFromControl', {appId: appId});
          }
          break;
        case "CloseWidget":
          if (isMaster) {
            wsio.emit('hideWidgetFromControl', {instanceID: data.ctrl.instanceID});
          }
          break;
        case "ShareApp":
          console.log("SHARE APP");
          break;
        default:
          app.SAGE2Event("widgetEvent", null, data.user, {identifier: ctrlId, action: action}, new Date(data.date));
          break;
      }

      // Check whether a request for clone was made.
      if (app.cloneable === true && app.requestForClone === true) {
        app.requestForClone = false;
        // console.log("cloning app:", appId, app.cloneData);
        if (isMaster) {
          wsio.emit('createAppClone', {id: appId, cloneData: app.cloneData});
        }
      }

    }

  });

  wsio.on('sliderKnobLockAction', function(data) {
    var ctrl   = getWidgetControlInstanceById(data.ctrl);
    var slider = ctrl.parent();
    var appId = data.ctrl.appId;
    var app = applications[appId];
    var ctrlId = slider.attr("id").replace("slider", "");
    app.SAGE2Event("widgetEvent", null, data.user, {identifier: ctrlId, action: "sliderLock"}, new Date(data.date));
    var ctrHandle    = document.getElementById(slider.data("instanceID"));
    var widgetOffset = ctrHandle ? parseInt(ctrHandle.style.left) : 0;
    var pos = data.x - ui.offsetX - widgetOffset;
    var sliderKnob = slider.select("rect");
    var knobWidthHalf = parseInt(sliderKnob.attr("width")) / 2;
    var knobCenterX   = parseInt(sliderKnob.attr("x")) + knobWidthHalf;
    if (Math.abs(pos - knobCenterX) > knobWidthHalf) {
      var updatedSliderInfo = mapMoveToSlider(sliderKnob, pos);
      var appObj = getPropertyHandle(applications[slider.data("appId")], slider.data("appProperty"));
      appObj.handle[appObj.property] = updatedSliderInfo.sliderValue;
      app.SAGE2Event("widgetEvent", null, data.user, {identifier: ctrlId, action: "sliderUpdate"}, new Date(data.date));
    }
  });

  wsio.on('moveSliderKnob', function(data) {
    // TODO: add `date` to `data` object
    //       DON'T USE `new Date()` CLIENT SIDE (apps will get out of sync)

    var ctrl = getWidgetControlInstanceById(data.ctrl);
    var slider = ctrl.parent();
    var ctrHandle = document.getElementById(slider.data("instanceID"));
    var widgetOffset = ctrHandle ? parseInt(ctrHandle.style.left) : 0;
    var pos = data.x - ui.offsetX - widgetOffset;
    var sliderKnob = slider.select("rect");
    var updatedSliderInfo = mapMoveToSlider(sliderKnob, pos);
    var appObj = getPropertyHandle(applications[slider.data("appId")], slider.data("appProperty"));
    appObj.handle[appObj.property] = updatedSliderInfo.sliderValue;
    var appId  = data.ctrl.appId;
    var app    = applications[appId];
    var ctrlId = slider.attr("id").replace("slider", "");
    app.SAGE2Event("widgetEvent", null, data.user, {identifier: ctrlId, action: "sliderUpdate"}, new Date(data.date));
  });

  wsio.on('keyInTextInputWidget', function(data) {
    // TODO: add `date` to `data` object
    //       DON'T USE `new Date()` CLIENT SIDE (apps will get out of sync)

    var ctrl = getWidgetControlInstanceById(data);
    if (ctrl) {
      var textInput = ctrl.parent();
      if (data.code !== 13) {
        insertTextIntoTextInputWidget(textInput, data.code, data.printable);
      } else {
        var ctrlId = textInput.attr("id").replace("textInput", "");
        var blinkControlHandle = textInput.data("blinkControlHandle");
        clearInterval(blinkControlHandle);
        var app = applications[data.appId];
        app.SAGE2Event("widgetEvent", null, data.user,
          {identifier: ctrlId, action: "textEnter", text: getTextFromTextInputWidget(textInput)}, new Date(data.date));
      }
    }
  });

  wsio.on('activateTextInputControl', function(data) {
    var ctrl = null;
    if (data.prevTextInput) {
      ctrl = getWidgetControlInstanceById(data.prevTextInput);
    }
    var textInput, blinkControlHandle;
    if (ctrl) {
      textInput = ctrl.parent();
      blinkControlHandle = textInput.data("blinkControlHandle");
      clearInterval(blinkControlHandle);
    }
    ctrl = getWidgetControlInstanceById(data.curTextInput);
    if (ctrl) {
      textInput = ctrl.parent();
      blinkControlHandle = setInterval(textInput.data("blinkCallback"), 1000);
      textInput.data("blinkControlHandle", blinkControlHandle);
    }
  });

  // Called when the user clicks outside the widget control while a lock exists on text input
  wsio.on('deactivateTextInputControl', function(data) {
    var ctrl = getWidgetControlInstanceById(data);
    if (ctrl) {
      var textInput = ctrl.parent();
      var blinkControlHandle = textInput.data("blinkControlHandle");
      clearInterval(blinkControlHandle);
    }
  });

  wsio.on('requestedDataSharingSession', function(data) {
    ui.showDataSharingRequestDialog(data);
  });

  wsio.on('closeRequestDataSharingDialog', function(data) {
    ui.hideDataSharingRequestDialog();
  });

  wsio.on('dataSharingConnectionWait', function(data) {
    ui.showDataSharingWaitingDialog(data);
  });

  wsio.on('closeDataSharingWaitDialog', function(data) {
    ui.hideDataSharingWaitingDialog();
  });

  wsio.on('initializeDataSharingSession', function(data) {
    console.log(data);
    dataSharingPortals[data.id] = new DataSharing(data);
  });
}


function createAppWindow(data, parentId, titleBarHeight, titleTextSize, offsetX, offsetY) {
  resetIdle();

  var parent = document.getElementById("appWindows");

  var date = new Date(data.date);
  var translate = "translate(0px, 0px)";

  var windowTitle = document.createElement("div");
  windowTitle.id  = data.id + "_title";
  windowTitle.className    = "windowTitle";
  windowTitle.style.visibility = 'hidden';
  windowTitle.style.width  = data.width.toString() + "px";
  // windowTitle.style.height = titleBarHeight.toString() + "px";
  windowTitle.style.height = "0px";
  windowTitle.style.left   = (-offsetX).toString() + "px";
  windowTitle.style.top    = (-offsetY).toString() + "px";
  windowTitle.style.webkitTransform = translate;
  windowTitle.style.mozTransform    = translate;
  windowTitle.style.transform       = translate;
  windowTitle.style.zIndex = itemCount.toString();

  var windowItem = document.createElement("div");
  windowItem.id = data.id;
  windowItem.className      = "windowItem";
  windowItem.style.left     = "0px";
  windowItem.style.top      = "0px";
  windowItem.style.webkitTransform = translate;
  windowItem.style.mozTransform    = translate;
  windowItem.style.transform       = translate;
  windowItem.style.overflow = "hidden";
  windowItem.style.zIndex   = "0";
  windowItem.style.position = "absolute";

  var titleText = document.createElement("p");
  titleText.id  = data.id + "_text";
  titleText.style.lineHeight = Math.round(titleBarHeight) + "px";
  titleText.style.fontSize   = Math.round(titleTextSize) + "px";
  titleText.style.color      = "#FFFFFF";
  titleText.style.marginLeft = Math.round(titleBarHeight / 4) + "px";
  titleText.textContent      = data.title;
  titleText.style.display = "none";

  var windowState = document.createElement("div");
  windowState.id = data.id + "_state";
  windowState.style.position = "absolute";
  windowState.style.width  = data.width.toString() + "px";
  windowState.style.height = data.height.toString() + "px";
  windowState.style.backgroundColor = "rgba(0,0,0,0.8)";
  windowState.style.lineHeight = Math.round(1.5 * titleTextSize) + "px";
  windowState.style.zIndex = "100";
  windowState.style.display = "none";

  var windowStateContatiner = document.createElement("div");
  windowStateContatiner.id = data.id + "_statecontainer";
  windowStateContatiner.style.position = "absolute";
  windowStateContatiner.style.top = "0px";
  windowStateContatiner.style.left = "0px";
  windowStateContatiner.style.webkitTransform = "translate(0px,0px)";
  windowStateContatiner.style.mozTransform = "translate(0px,0px)";
  windowStateContatiner.style.transform = "translate(0px,0px)";
  
  parent.appendChild(windowItem);
  windowState.appendChild(windowStateContatiner);
  windowItem.appendChild(windowState);
  windowItem.appendChild(titleText);
  windowItem.appendChild(windowTitle);

  // App launched in window
  if (data.application === "media_stream") {
    wsio.emit('receivedMediaStreamFrame', {id: data.id});
  }
  if (data.application === "media_block_stream") {
    wsio.emit('receivedMediaBlockStreamFrame', {id: data.id, newClient: true});
  }

  // convert url if hostname is alias for current origin
  var url = cleanURL(data.url);

  function loadApplication() {
    var init = {
      id: data.id,
      x: data.left,
      y: data.top + titleBarHeight,
      width: data.width,
      height: data.height,
      resrc: url,
      state: data.data,
      date: date,
      title: data.title
    };

    // load new app
    if (window[data.application] === undefined) {
      var js = document.createElement("script");
      js.addEventListener('error', function(event) {
        console.log("Error loading script: " + data.application + ".js");
      }, false);
      js.addEventListener('load', function(event) {
        var newapp = new window[data.application]();
        newapp.init(init);
        newapp.refresh(date);

        applications[data.id]   = newapp;
        controlObjects[data.id] = newapp;

        if (data.animation === true) {
          wsio.emit('finishedRenderingAppFrame', {id: data.id});
        }
      }, false);
      js.type  = "text/javascript";
      js.async = false;
      js.src = url + "/" + data.application + ".js";
      console.log(data.id, url + "/" + data.application + ".js");
      document.head.appendChild(js);
    } else {
      // load existing app
      var app = new window[data.application]();
      app.init(init);
      // app.SAGE2Load(app.state, date);
      app.refresh(date);

      applications[data.id] = app;
      controlObjects[data.id] = app;

      if (data.animation === true) {
        wsio.emit('finishedRenderingAppFrame', {id: data.id});
      }
      if (data.application === "movie_player") {
        setTimeout(function() { wsio.emit('requestVideoFrame', {id: data.id}); }, 500);
      }
    }
  }

  // load all dependencies
  if (data.resrc === undefined || data.resrc === null || data.resrc.length === 0) {
    loadApplication();
  } else {
    var loadResource = function(idx) {
      if (dependencies[data.resrc[idx]] !== undefined) {
        if ((idx + 1) < data.resrc.length) {
          loadResource(idx + 1);
        } else {
          console.log("all resources loaded", data.id);
          loadApplication();
        }

        return;
      }

      dependencies[data.resrc[idx]] = false;

      var js = document.createElement("script");
      js.addEventListener('error', function(event) {
        console.log("Error loading script: " + data.resrc[idx]);
      }, false);

      js.addEventListener('load', function(event) {
        dependencies[data.resrc[idx]] = true;

        if ((idx + 1) < data.resrc.length) {
          loadResource(idx + 1);
        } else {
          console.log("all resources loaded", data.id);
          loadApplication();
        }
      });
      js.type  = "text/javascript";
      js.async = false;
      if (data.resrc[idx].indexOf("http://")  === 0 ||
        data.resrc[idx].indexOf("https://") === 0 ||
        data.resrc[idx].indexOf("/") === 0) {
        js.src = data.resrc[idx];
      } else {
        js.src = url + "/" + data.resrc[idx];
      }
      document.head.appendChild(js);
    };
    // Start loading the first resource
    loadResource(0);
  }

  itemCount += 2;
  updateSelectBox();
  if (document.getElementsByClassName("windowItem").length == 1 ){
    // display default app (first in select box)
    windowItem.style.visibility = 'visible';
  }else{
    windowItem.style.visibility = 'hidden';
  }

  //disables the ability to control the applications
  windowItem.style.pointerEvents = "none";
  handleOrientationChange();
  appSelected();
}

function getTransform(elem) {
  var transform = elem.style.transform;
  var translate = {x: 0, y: 0};
  var scale = {x: 1, y: 1};
  if (transform) {
    var tIdx = transform.indexOf("translate");
    if (tIdx >= 0) {
      var tStr = transform.substring(tIdx + 10, transform.length);
      tStr = tStr.substring(0, tStr.indexOf(")"));
      var tValue = tStr.split(",");
      translate.x = parseFloat(tValue[0]);
      translate.y = parseFloat(tValue[1]);
    }
    var sIdx = transform.indexOf("scale");
    if (sIdx >= 0) {
      var sStr = transform.substring(sIdx + 6, transform.length);
      sStr = sStr.substring(0, sStr.indexOf(")"));
      var sValue = sStr.split(",");
      scale.x = parseFloat(sValue[0]);
      scale.y = parseFloat(sValue[1]);
    }
  }
  return {translate: translate, scale: scale};
}